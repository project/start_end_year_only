jQuery(document).ready(function () {

    jQuery('.start-end-year-popup .year-input').datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });

    jQuery('.start-end-year-popup .year-input').click(function() {
        const selectedID = jQuery(this).attr('id');
        jQuery(this).on('input', function () {
            this.value = ''; // Clear the value if any input is detected
          });
          // Enable the input element that was clicked
          jQuery('.start-end-year-popup .year-input').on('click', function () {
            jQuery(this).prop('disabled', false);
          });
    });
});
