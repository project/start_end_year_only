## Start End Year Only

## Contents of this file

 - Introduction
 - Requirements
 - Installation
 - Maintainers


## Introduction

  The Start End Year Only module enhances Drupal's date field functionality by introducing a specialized widget tailored for selecting start and end years. When creating a new date field, this module seamlessly integrates the Start End Year Only field type, simplifying the process of managing date ranges within your content.

## Requirements
  Enabled core module: Field and Text.

## Installation
  Install as usual, see: https://www.drupal.org/docs/extending-drupal/installing-modules for further information.

## Maintainers

  Current maintainers:
  - Adnan khan (dev-ad) - https://www.drupal.org/u/dev-ad
  - Mohammed Abdullah Bamlhes (bamlhes) - https://www.drupal.org/u/bamlhes

