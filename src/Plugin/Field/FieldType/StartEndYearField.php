<?php

namespace Drupal\start_end_year_only\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'start_end_year_only' field type.
 *
 * @FieldType(
 *   id = "start_end_year_only",
 *   label = @Translation("Start End Year Only"),
 *   category = "date_time",
 *   description = {
 *     @Translation("Allows users to specify start and end years conveniently using a date picker."),
 *     @Translation("The widget restricts selection to dates only, ensuring simplicity and clarity."),
 *     @Translation("Enforces the rule that the start year must always less the end year."),
 *   },
 *   default_widget = "start_end_year_only",
 *   default_formatter = "start_end_year_only",

 * )
 */
class StartEndYearField extends FieldItemBase
{

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition)
  {

    $properties = [];

    $properties['start_year'] = DataDefinition::create('string')
      ->setLabel(t('start_year'));
    $properties['end_year'] = DataDefinition::create('string')
      ->setLabel(t('end_year'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition)
  {
    return [
      'columns' => [
        'start_year' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'end_year' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }
}
