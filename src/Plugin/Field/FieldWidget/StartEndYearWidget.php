<?php

namespace Drupal\start_end_year_only\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'start_end_year_only' widget.
 *
 * @FieldWidget(
 *   id = "start_end_year_only",
 *   label = @Translation("Start End year only"),
 *   field_types = {
 *     "start_end_year_only"
 *   }
 * )
 */
class StartEndYearWidget extends WidgetBase
{

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
  {
    $element = [
      '#theme_wrappers' => ['container', 'form_element'],
      '#attributes' => ['class' => ['start-end-year-popup', 'entity-meta__header accordion__item js-form-wrapper form-wrapper']],
      '#attached' => [
        'library' => ['start_end_year_only/widget'],
      ],
    ];
    $field_name = $this->fieldDefinition->getLabel();
    $element['field_name'] = [
      '#markup' =>   "<h3 class='start-end-year-label'> $field_name</h3>",
    ];
    $element['start_year'] = [
      '#title' => $this->t('Start Year'),
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->end_year) ? $items[$delta]->end_year : '',
      '#size' => 15,
      '#maxlength' => 4,
      '#attributes' => ['class' => ['start-year-value', 'year-input']]

    ];
    $element['end_year'] = [
      '#title' => $this->t('End Year'),
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->end_year) ? $items[$delta]->end_year : '',
      '#size' => 15,
      '#maxlength' => 4,
      '#attributes' => ['class' => ['end-year-value', 'year-input']]
    ];

    $element['#element_validate'][] = [$this, 'validateStartEnd'];
    return $element;
  }


  /**
   * #element_validate callback to ensure that the start year < the end year.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public function validateStartEnd(array &$element, FormStateInterface $form_state, array &$complete_form)
  {
    $start_year = $element['start_year']['#value'];
    $end_year = $element['end_year']['#value'];
    if ($start_year >  $end_year) {
      $form_state->setError($element, $this->t('Start year should be less than end year.'));
    }
  }
}
