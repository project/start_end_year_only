<?php

namespace Drupal\start_end_year_only\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'start_end_year_only' formatter.
 *
 * @FieldFormatter(
 *   id = "start_end_year_only",
 *   label = @Translation("start end year only"),
 *   field_types = {
 *     "start_end_year_only"
 *   }
 * )
 */
class MarkupFormatter extends FormatterBase
{

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode)
  {

    $elements = [];
    foreach ($items as $delta => $item) {

      $start_year = $item->start_year;
      $end_year = $item->end_year;
      $elements[$delta] = [
        '#markup' =>  $start_year . ' - '  . $end_year
      ];
    }

    return $elements;
  }
}
